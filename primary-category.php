<?php

/**
 * Primary category bootstrap file
 *
 * @since             1.0.0
 * @package           Primary_Category
 *
 * @wordpress-plugin
 * Plugin Name:       Primary Category
 * Plugin URI:        https://okayididntreallybuyadomainforthisplugin.com
 * Description:       This plugin allows you to set the primary category for a post.
 * Version:           1.0.0
 * Author:            Joshua Flowers
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       primary-category
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin version.
 */
define( 'PRIMARY_CATEGORY_VERSION', '1.0.0' );

/**
 * Core plugin class used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-primary-category.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_primary_category() {

	$plugin = new Primary_Category();
	$plugin->run();

}

/**
 * Run Primary Category on admin post pages
 */
if (is_admin()) {
	run_primary_category();
}
