<?php

/**
 * Load and define the internationalization files for this plugin
 *
 * @since      1.0.0
 * @package    Primary_Category
 * @subpackage Primary_Category/includes
 * @author     Joshua Flowers <joshua@echo5digital.com>
 */
class Primary_Category_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'primary-category',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
