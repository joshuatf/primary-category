<?php

/**
 * The core plugin class.
 *
 * Define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * @since      1.0.0
 * @package    Primary_Category
 * @subpackage Primary_Category/includes
 * @author     Joshua Flowers <joshua@echo5digital.com>
 */
class Primary_Category {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Primary_Category_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PRIMARY_CATEGORY_VERSION' ) ) {
			$this->version = PRIMARY_CATEGORY_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'primary-category';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Primary_Category_Loader. Orchestrates the hooks of the plugin.
	 * - Primary_Category_i18n. Defines internationalization functionality.
	 * - Primary_Category_Admin. Defines all hooks for the admin area.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-primary-category-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-primary-category-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-primary-category-admin.php';

		$this->loader = new Primary_Category_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Primary_Category_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Primary_Category_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Primary_Category_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_update_primary_category', $plugin_admin, 'update_primary_category' );
		$this->loader->add_action( 'site_transient_update_plugins', $plugin_admin, 'remove_wp_repo_conflicts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Primary_Category_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Get all the posts for a primary category
	 *
	 * @since     1.0.0
	 * @param	  int	$post_id	The post ID
	 * @return    WP_Query
	 */
	public static function get_posts($category_id, $custom_args = array()) {
		$args = array(
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'primary_category',
					'value' => $category_id,
					'compare' => '=',
				)
			)
		);
		$merged_args = array_merge($args, $custom_args);
		return new WP_Query($merged_args);
	}

	/**
	 * Get the primary category ID for a post
	 *
	 * @since     1.0.0
	 * @param	  int	$post_id	The post ID
	 * @return    string    The category name
	 */
	public static function get_primary_category($post_id = 0) {
		$post_id = $post_id == 0 ? get_the_ID() : $post_id;		
		return get_post_meta( $post_id, 'primary_category', true );
	}

		/**
	 * Get the primary category name for a post
	 *
	 * @since     1.0.0
	 * @param	  int	$post_id	The post ID
	 * @return    string    The category name
	 */
	public static function get_primary_category_name($post_id = 0) {
		return get_the_category_by_ID(intval($this->get_primary_category($post_id)));
	}

	/**
	 * Set the primary category for a post
	 * Post ID will use current post ID if not set
	 *
	 * @since     1.0.0
	 * @param	  int	$category_id	The Category ID
	 * @param	  int	$post_id	The ID of the post to update
	 */
	public static function set_primary_category($category_id, $post_id = 0) {
		$post_id = $post_id == 0 ? get_the_ID() : $post_id;
		wp_set_post_categories($post_id, array($category_id), true);
		update_post_meta($post_id, 'primary_category', $category_id);
	}

}
