# Primary Category

This plugin allows you to assign a primary category to posts.

![Screenshot](assets/screenshot_1.png)

## Description

Primary Category will add a button next to the category list that allows categories to be marked as primary.

Categories marked as primary are automatically added to the post's categories list via AJAX.

Note that this doesn't affect WordPress's default category behavior and categories that are checked/unchecked manually will require the post to be saved as usual.

## Usage

This plugin exposes a few functions for setting and retrieving primary categories.  By default if the `$post_id` parameter is ommitted, it will attempt to use the current global post ID.

To retreive the primary category ID for a post:

```php
Primary_Category::get_primary_category($post_id);
```

To retreive the primary category name for a post:

```php
Primary_Category::get_primary_category_name($post_id);
```

To set a primary category for a post:

```php
Primary_Category::set_primary_category($category_id, $post_id);
```

To get the posts for a primary category with optional arguments for WP_Query as the second parameter:

```php
Primary_Category::get_posts($category_id, array());
```

The above will return an instance of WP_Query to be used to loop over posts from the primary category.  This allows better integration with front-end templates and access to WP core features, such as pagination and the WordPress loop.  For example:

```php
$query = Primary_Category::get_posts(6);
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
        // Show titles of all posts with a primary category ID of 6
		$query->the_post();
		echo get_the_title();
	}
	wp_reset_postdata();
}
```

To retrieve posts with a custom post type from a primary category, you can pass in the post type in the arguments list:

```php
Primary_Category::get_posts($category_id, array(
    'post_type' => 'my_custom_post_type'
));
```