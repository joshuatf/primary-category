<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    Primary_Category
 * @subpackage Primary_Category/admin
 * @author     Joshua Flowers <joshua@echo5digital.com>
 */
class Primary_Category_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/primary-category-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Remove update nag for WP repo with similar name
	 *
	 * @since    1.0.0
	 */
	public function remove_wp_repo_conflicts($value) {
		if( isset( $value->response['primary-category/primary-category.php'] ) ) {
			unset( $value->response['primary-category/primary-category.php'] );
		}
		return $value;
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/primary-category-admin.js', array( 'jquery' ), $this->version, false );
		$params = array(
			'ajaxUrl' => admin_url('admin-ajax.php'),
			'ajaxNonce' => wp_create_nonce('primary-category'),
			'primaryText' => esc_attr('Primary', 'primary-category'),
			'makePrimaryText' => esc_attr('Make Primary', 'primary-category'),
			'primaryCategoryId' => Primary_Category::get_primary_category(),
			'postId' => get_the_ID()
		);
		wp_localize_script( $this->plugin_name, 'pc_params', $params );

	}

	/**
	 * Update a post's category via AJAX
	 *
	 * @since    1.0.0
	 */
	public function update_primary_category() {

		check_ajax_referer( 'primary-category', 'security' );
		$category_id = intval($_POST['category_id']);
		$post_id = intval($_POST['post_id']);
		Primary_Category::set_primary_category($category_id, $post_id);
		wp_die();

	}

}
