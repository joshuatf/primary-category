(function( $ ) {
	'use strict';

	/**
	 * Add 'Make Primary' buttons to categories
	 */
	function updateCategoryList() {
		$('#categorychecklist li').each(function() {
			var categoryId = $(this).find('input').val();
			if (categoryId == pc_params.primaryCategoryId) {
				var buttonHtml = '<span class="make-primary primary">' + pc_params.primaryText + '<span>';
			} else {
				var buttonHtml = '<button class="make-primary" data-category-id="' + categoryId + '">' + pc_params.makePrimaryText + '</button>';
			}
			var button = $(this).find('.make-primary');
			if (button.length) {
				button.replaceWith(buttonHtml)
			} else {
				$(this).append(buttonHtml);
			}
		});
	}

	/**	
	 * Update the current post's primary category
	 */
	function updatePostPrimaryCategory(categoryId) {
		var data = {
			action: 'update_primary_category',
			security: pc_params.ajaxNonce,
			category_id: categoryId,
			post_id: pc_params.postId
		};
		$.ajax({
			type : "post",
			dataType : "json",
			url : pc_params.ajaxUrl,
			data : data,
			success: function(response) {
				pc_params.primaryCategoryId = categoryId;
				$('input[name="post_category[]"][value="' + categoryId + '"]').attr('checked', 'checked');
			}
		});
	}

	/**
	 * Add buttons for categories added via AJAX
	 */
	$(document).ajaxSuccess(function() {
		updateCategoryList();
	});

	/**
	 * Click handler for buttons
	 */
	$(document).on('click', '.make-primary', function(e) {
		e.preventDefault();
		var categoryId = $(this).data('category-id');
		updatePostPrimaryCategory(categoryId);
	});

	/**
	 * Initialize buttons on load
	 */
	$(window).load(function() {
		updateCategoryList();
	});

})( jQuery );
